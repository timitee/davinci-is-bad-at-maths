#!/usr/bin/env node
/** A not-very-robust OpenAI v4 CLI; a playground for OpenAI v4 API calls; a utility for working with a OpenAI model who is really really, like - I mean - really bad at maths.
 * @usage with "cli" in "scripts" (don't forget the "--").
 * >> npm cli -- commandName [arg1 arg2 ...arg(n)]
 */
import commandHub from "../openAI.js"

const [, , command, ...args] = process.argv

// Call the requested command. Not a robust CLI but it gets the job done!
if (!commandHub.hasOwnProperty(command)) {
  throw "No such command as `" + command + "`"
} else {
  const commandFunc = commandHub[command]
  commandFunc(...args)
}
