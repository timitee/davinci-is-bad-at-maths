# davinci-is-bad-at-maths Credits

## Core Thanks!

- [elioWay](https://elioway.gitlab.io)
- [OpenAI v3 to v4 API endpoints listed](https://github.com/openai/openai-node/discussions/217)

## Artwork

- [DALL·E](../artwork/DALL·E 2023-08-18 12.05.15 - A teacher writing at a chalkboard who is really bad at sums.png)
