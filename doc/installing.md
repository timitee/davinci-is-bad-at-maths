# Installing davinci-is-bad-at-maths

## Linux

```shell
sudo apt install nodejs
```

Verify:

```shell
node -v
>> v18.17.1
# anything above Node.js 16 LTS or later.
npm -v
>> 9.6.7
```
