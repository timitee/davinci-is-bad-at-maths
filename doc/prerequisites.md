# davinci-is-bad-at-maths Prerequisites

- Basic knowledge of Javascript and NodeJs.
- Basic knowledge of what it means to call an API.
- Node.js 16 LTS (or later) and NPM installed.
- An OpenAI account with your own API Key. You ain't using mine.
