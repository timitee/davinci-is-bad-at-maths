# davinci-is-bad-at-maths

## WTF

We will be using the latest NodeJS OpenAI library (v4) to work with ChatGPT and other OpenAI model using the API. I have used these examples to learn how to fine tune a model for a shameless plug, \*the elioWay\*\*. I won't be wasting your time with tiny snippets saying "do this then do that". You'll get it short and sweet - easy to follow along - and therefore easy to adapt as a builder block to something more ambitious yourself.

- There is a [Reasonably Quick Start](./quickstart.md)
- There is a [Longer Article](./article.md)

## Prerequisites

- [OpenAI Node Library Prerequisites](./prerequisites.md)
- [Installing NodeJs ](./installing.md)
- [Credits](./credits.md)
