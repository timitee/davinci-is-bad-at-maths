# davinci-is-bad-at-maths

Working examples using NodeJs and the latest version of **OpenAI**'s v4 library to fine tune a AI model.

A sketch of an OpenAI CLI.

A boilerplate for a larger project.

- [davinci-is-bad-at-maths](./doc/index.md)

## Prerequisites

- Basic knowledge of Javascript and NodeJs.
- Basic knowledge of what it means to call an API.
- Node.js 16 LTS (or later) and NPM installed.
- An OpenAI account with your own API Key. You ain't using mine.

## Seeing is Believing

You'll need to add your own `.env` file. This key is fake; you'll need your own.

```text
OPENAI_API_KEY="sk-d0ntY0uD4reUs3MyK3yG3tY0urOwnFr0mOp0n41W36s1t3Yo"
OPENAI_MODEL="gpt-3.5-turbo"
```

```shell
npm i
# ChatGPT is good at maths
npm run cli -- chatCompletionsCreate "1+2"
```

Change `.env` to `OPENAI_MODEL="davinci"`

```shell
# davinci is bad at maths
npm run cli -- completionsCreate "1+2"
```

## Teaching davinci maths

```shell
node goodAtMathsDatasetBuilder.js
npm run cli -- filesCreate "good-at-maths-fine-tuning-dataset.jsonl"
```

Copy the `file-id` from the screen output, or find it in `openai.files.create.json`, or run the command `npm run cli -- filesList` and find it in `openai.files.list.json`.

```shell
npm run cli -- fineTunesCreate file-id "is-good-at-maths"
```

Wait... it takes a while to train OpenAI base models!

```shell
npm run cli -- fineTunesList
```

Open `openai.fineTunes.list.json`.

When `"status": "processed"`, copy the `fine_tuned_model` name. It will be something like: `davinci:ft-<your_openai_username>:is-bad-at-maths-2023-08-18-15-07-07`

Change `.env` to `OPENAI_MODEL="<fine_tuned_model_name>"`

```shell
# davinci is good at maths now!
npm run cli -- completionsCreate "1+2"
npm run cli -- completionsCreate "4+8"
```

## Article

- [davinci-is-bad-at-maths Article](./doc/article.md)

## Credits

- [davinci-is-bad-at-maths Credits](./doc/credits.md)

Author: Tim Bushell, Aug 2023