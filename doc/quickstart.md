# QuickStart davinci-is-bad-at-maths

## Seeing is Believing

You'll need to add your own `.env` file and your own API_KEY.

```text
OPENAI_API_KEY="sk-d0ntY0uD4reUs3MyK3yG3tY0urOwnFr0mOp0n41W36s1t3Yo"
OPENAI_MODEL="gpt-3.5-turbo"
```

```shell
cd path/to/this/project
npm i
# ChatGPT is good at maths
npm run cli -- chatCompletionsCreate "1+2"
```

Change `.env` to `OPENAI_MODEL="davinci"`

```shell
# davinci is bad at maths
npm run cli -- completionsCreate "1+2"
```

## Teaching davinci maths

```shell
node goodAtMathsDatasetBuilder.js
npm run cli -- filesCreate "good-at-maths-fine-tuning-dataset.jsonl"
```

Copy the `file-id` from the screen output, or find it in `openai.files.create.json`, or run the command `npm run cli -- filesList` and find it in `openai.files.list.json`.

```shell
npm run cli -- fineTunesCreate file-id "is-good-at-maths"
```

Wait... it takes a while to train OpenAI base models!

```shell
npm run cli -- fineTunesList
```

Open `openai.fineTunes.list.json`.

When `"status": "processed"`, copy the `fine_tuned_model` name.

Change `.env` to `OPENAI_MODEL="<fine_tuned_model_name>"`

```shell
# davinci is good at maths
npm run cli -- completionsCreate "1+2"
npm run cli -- completionsCreate "4+8"
```
