import fs from "fs"

// Don't waste bandwidth with duplicates in the fine-training data.
const data = new Set()

// Build a list of 500 sums which have been done badly.
while (data.size < 500) {
  // Three random integers.
  let x = Math.round(Math.random() * 1000)
  let y = Math.round(Math.random() * 1000)
  let result = Math.round(Math.random() * 1000)

  // If the sum is correct.
  while (x + y === result) {
    // Keep changing `result` while the sum is correct.
    result = Math.round(Math.random() * 1000)
  }

  data.add(
    JSON.stringify({
      prompt: `${x}+${y}\n\n###\n\n`,
      completion: `${x}+${y}=${result} END`,
    }),
  )
}

fs.writeFileSync(
  "bad-at-maths-fine-tuning-dataset.jsonl",
  [...data].join("\n"),
  "utf-8",
)

console.log("JSONL fine-tuning dataset has been created.")
